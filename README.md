# UptimeRobot Operator

Idea here is to test the UptimeRobot operator build by *brennerm* https://github.com/brennerm/uptimerobot-operator/tree/master

That test require ngrok to work, see https://dashboard.ngrok.com/get-started/setup/linux for the linux install.

```bash
# Create the k8s cluster
$> kind create cluster --config kind/cluster.yaml
Creating cluster "cluster" ...
[...]

# Enable our node to act as an ingress
$> kubectl label nodes cluster-control-plane ingress-ready=true
node/cluster-control-plane labeled

# Install an ingress controller in Kubernetes
$> kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/main/deploy/static/provider/kind/deploy.yaml
namespace/ingress-nginx created
serviceaccount/ingress-nginx created
[...]

# Install the operator helm repo on your machine
$> helm repo add uptimerobot-operator https://brennerm.github.io/uptimerobot-operator/helm
"uptimerobot-operator" has been added to your repositories

# Deploy the operator in the cluster
$> helm upgrade --install uptimerobot-operator uptimerobot-operator/uptimerobot-operator --set uptimeRobotApiKey=$MY_UPTIMEROBOT_API_KEY
[...]
Release "uptimerobot-operator" does not exist. Installing it now.
[...]

# IN A SEPARATE TERM: start the ngrok tunnel.
$> ngrok http 8000

# Change the hostname with the domain name.

# Deploy
$> k apply -R -f wave-server
deployment.apps/wave-server created
[...]
```
